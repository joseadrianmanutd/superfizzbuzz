﻿using FizzBuzz.Library;
using System;
using System.Collections.Generic;

namespace SuperFizzBuzz.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 3, "Fizz" },
                { 7, "Buzz" },
                { 38, "Bazz" }
            };

            var fizzBuzzService = new FizzBuzz.Library.SuperFizzBuzz();
            foreach (var value in fizzBuzzService.Generate(-12, 145, numbersMappingDictionary))
            {
                Console.WriteLine(value);
            }
            Console.ReadLine();
        }
    }
}
