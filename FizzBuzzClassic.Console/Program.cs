﻿using FizzBuzz.Library;
using System;
using System.Collections.Generic;

namespace FizzBuzzClassic.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 3, "Fizz" },
                { 5, "Buzz" }
            };

            var fizzBuzzService = new SuperFizzBuzz();
            foreach (var value in fizzBuzzService.Generate(1, 100, numbersMappingDictionary))
            {
                Console.WriteLine(value);
            }
            Console.ReadLine();
        }
    }
}
