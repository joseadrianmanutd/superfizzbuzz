using FizzBuzz.Library;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz.Tests
{
    public class FizzBuzzTests
    {
        private SuperFizzBuzz _service;

        [SetUp]
        public void Setup()
        {
            _service = new SuperFizzBuzz();
        }

        [Test]
        public void FizzBuzz_Returns_Numbers_When_Null_Mappings()
        {
            var start = 1;
            var end = 10;
           
            var output = _service.Generate(start, end, null);
            Assert.That(output.All(s => int.TryParse(s, out int n)), Is.True);
        }

        [Test]
        public void FizzBuzz_Returns_FrogDuckChicken_When_Number_Is_Divisible_By_4_and_13_and_9()
        {
            var start = 0;
            var end = 500;
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 4, "Frog" },
                { 13, "Duck" },
                { 9, "Chicken" }
            };

            var output = _service.Generate(start, end, numbersMappingDictionary);
            Assert.That(output.ElementAt(52), Is.EqualTo("FrogDuck"));
            Assert.That(output.ElementAt(36), Is.EqualTo("FrogChicken"));
            Assert.That(output.ElementAt(468), Is.EqualTo("FrogDuckChicken"));
        }

        [Test]
        public void FizzBuzz_Returns_Fizz_When_Number_Is_Divisible_By_Only_3()
        {
            var start = 1;
            var end = 10;
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 3, "Fizz" }
            };

            var output = _service.Generate(start, end, numbersMappingDictionary);
            Assert.That(output.ElementAt(2), Is.EqualTo("Fizz"));
            Assert.That(output.ElementAt(5), Is.EqualTo("Fizz"));
            Assert.That(output.ElementAt(8), Is.EqualTo("Fizz"));
        }

        [Test]
        public void FizzBuzz_Returns_Buzz_When_Number_Is_Divisible_By_Only_5()
        {
            var start = 1;
            var end = 10;
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 5, "Buzz" }
            };

            var output = _service.Generate(start, end, numbersMappingDictionary);
            Assert.That(output.ElementAt(4), Is.EqualTo("Buzz"));
            Assert.That(output.ElementAt(9), Is.EqualTo("Buzz"));
        }

        [Test]
        public void FizzBuzz_Returns_FizzBuzzBazz_When_Number_Is_Divisible_By_3_and_7_and_38()
        {
            var start = -10;
            var end = 10;
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 3, "Fizz" },
                { 7, "Buzz" },
                { 38, "Bazz" }
            };

            var output = _service.Generate(start, end, numbersMappingDictionary);
            Assert.That(output.ElementAt(10), Is.EqualTo("FizzBuzzBazz"));
        }

        [Test]
        public void FizzBuzz_Returns_Number_When_Number_Is_Not_Divisible_By_3_or_7_or_38()
        {
            var start = -10;
            var end = 0;
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 3, "Fizz" },
                { 7, "Buzz" },
                { 38, "Bazz" }
            };

            var output = _service.Generate(start, end, numbersMappingDictionary);
            Assert.That(output.ElementAt(2), Is.EqualTo("-8"));
            Assert.That(output.ElementAt(5), Is.EqualTo("-5"));
            Assert.That(output.ElementAt(6), Is.EqualTo("-4"));
            Assert.That(output.ElementAt(8), Is.EqualTo("-2"));
            Assert.That(output.ElementAt(9), Is.EqualTo("-1"));
        }

        public void FizzBuzz_Custom_Set_Int_Returns_Fizz_When_Number_Is_Divisible_By_Only_3()
        {
            var input = new List<int> { 3, 6, 9, 12 };
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 3, "Fizz" }
            };

            var output = _service.Generate(input, numbersMappingDictionary);
            Assert.That(output.All(s => s == "Fizz"), Is.True);
        }

        [Test]
        public void FizzBuzz_Custom_Set_Int_Returns_Buzz_When_Number_Is_Divisible_By_Only_5()
        {
            var input = new List<int> { 0, 5, 20, 100 };
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 5, "Buzz" }
            };

            var output = _service.Generate(input, numbersMappingDictionary);
            Assert.That(output.All(s => s == "Buzz"), Is.True);
        }

        [Test]
        public void FizzBuzz_Custom_Set_Int_Returns_FizzBuzz_When_Number_Is_Divisible_By_3_and_7()
        {
            var input = new List<int> { 10, 21, 42, 43 };
            var numbersMappingDictionary =
            new Dictionary<int, string>
            {
                { 3, "Fizz" },
                { 7, "Buzz" }
            };

            var output = _service.Generate(input, numbersMappingDictionary);
            Assert.That(output.ElementAt(0), Does.Not.Match("FizzBuzz"));
            Assert.That(output.ElementAt(1), Does.Match("FizzBuzz"));
            Assert.That(output.ElementAt(2), Does.Match("FizzBuzz"));
            Assert.That(output.ElementAt(3), Does.Not.Match("FizzBuzz"));
        }
    }
}