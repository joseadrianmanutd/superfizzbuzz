﻿using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz.Library
{
    public class SuperFizzBuzz
    {
        //Receive the start and end numbers and generates a collection of ints that are consecutive
        public IEnumerable<string> Generate(int start, int end, Dictionary<int, string> numbersMappings = null)
        {
            return Generate(Enumerable.Range(start, end - start + 1), numbersMappings);
        }

        //Receive a list of ints that may not be consecutive
        public IEnumerable<string> Generate(IEnumerable<int> numbers, Dictionary<int, string> numbersMappings = null)
        {
            //ckeck for null to the mappings (this will result in the original list of numbers to be returned) 
            if (numbersMappings == null)
            {
                return numbers.Select(n => n.ToString());
            }

            return GenerateFizzBuzz(numbers, numbersMappings);
        }

        private IEnumerable<string> GenerateFizzBuzz(IEnumerable<int> numbers, Dictionary<int, string> numbersMappings = null)
        {
            foreach (var number in numbers)
            {
                //Get a list of strings from the mapping collection where the modulus of current number is zero
                var names = numbersMappings.Where(i => number % i.Key == 0)
                    .Select(i => i.Value);

                //Add to the final result an string with the names found in the collection or add the number if collection was empty
                yield return names.Any() ? string.Join("", names) : number.ToString();
            }
        }
    }
}
